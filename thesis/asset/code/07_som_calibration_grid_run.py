
# coding: utf-8

# # 07 - SOM calibration - Grid run

# In[37]:

import os
import math
import pickle
import pandas as pd
import numpy as np
import scipy as sp
import scipy.constants as const
from som import som
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
get_ipython().magic(u'matplotlib inline')
from IPython.display import display, display_markdown
from random import shuffle
from datetime import datetime as dt
from datetime import timedelta as td


# ## Functions

# In[38]:

def slope(x1, x2, y1, y2):
    return (y2-y1)/(x2-x1)

def angle(x1, x2, y1, y2):
    return math.degrees(math.atan((x2-x1)/(y2-y1)))
    
def slopeLine(x,y):
    m = []
    for i in range(len(x)):
        if i == x[0]:
            m.append(np.nan)
        else:
            m.append(slope(x[i-1],x[i],y[i-1],y[i]))
    return m

def angleLine(x,y):
    m = []
    for i in range(len(x)):
        if i == x[0]:
            m.append(np.nan)
        else:
            m.append(angle(x[i-1],x[i],y[i-1],y[i]))
    return m


# ## Données de stéroïdes

# In[39]:

steroids = pickle.load(open("output/01_steroids_cleaned.pickle", "rb"))

attributes = [
        'cr_saad3a17b_d', 'cr_ll_oxo_etio_d', 'cr_llb_oh_andro_d', 'cr_llb_oh_etio_d', 'cr_dha_d',
        'cr_s_ad_17b_d', 'cr_l6a_oh_dha_d', 'cr_s_at_d', 'cr_s_pt_d', 'cr_testosterone_d', 'cr_sa_dihydrotest_d',
        'cr_estriol_d', 'cr_l7b_estradiol_d', 'cr_pd_d', 'cr_pt_d', 'cr_pt_one_d',
        'cr_ths_d', 'cr_thaldo_d', 'cr_thdoc_d', 'cr_tha_d', 'cr_thb_d', 'cr_sa_thb_d', 'cr_l8_oh_tha_d',
        'cr_cortisone_d', 'cr_the_d', 'cr_a_cortolone_d', 'cr_b_cortolone_d', 'cr_zoa_dhe_d', 'cr_zob_dhe_d',
        'cr_cortisol_d', 'cr_a_cortol_d', 'cr_b_cortol_d', 'cr_zoa_dhf_d', 'cr_sb_oh_f_d', 'cr_l8_oh_f_d'
]

attributes = [v.lower() for v in attributes]
print len(attributes)
print steroids.shape


# In[40]:

matrix_steroids = steroids.as_matrix()


# ## Exécution du batch de SOM

# In[41]:

## Execution parameters
runType = "test"

if runType == "test":
    n_iter_range = range(10, 24, 2)
    dim_range = range(20, 50, 10)
    nb_exec = range(1)
    
elif runType == "lowRes":
    n_iter_range = range(20, 120, 20)
    dim_range = range(30, 330, 30)
    nb_exec = range(3)
    
elif runType == "hiRes":
    n_iter_range = range(10, 110, 10) # To be defined
    dim_range = range(10, 110, 10) # To be defined
    nb_exec = range(10)

learning_rate = (0.09, 0.02)
neighborhood_size = "((1./2)*int(np.ceil(const.golden*dim)), 1)"


# In[19]:

# Save parameter in a file
it_step = n_iter_range[1]-n_iter_range[0]
it_start = n_iter_range[0]
it_stop = n_iter_range[-1]+it_step

dim_step = dim_range[1]-xdim_range[0]
dim_start = dim_range[0]
dim_stop = dim_range[-1]+dim_step

str1 = "n_iter_range = range("+str(it_start)+", "+str(it_stop)+", "+str(it_step)+")"
str2 = "xdim_range = range("+str(dim_start)+", "+str(dim_stop)+", "+str(dim_step)+")"
str3 = "nb_exec = range("+str(len(nb_exec))+")"
str4 = "learning_rate = "+str(learning_rate)
str5 = "neighborhood_size = "+str(neighborhood_size) 

text = str1+"\n"+str2+"\n"+str3+"\n"+str4+"\n"+str5+"\n\n"

now = dt.now()

# Set the different output file names
report_file = 'output/'+runType+'/'+now.strftime("%y%m%d_%H%M")+'_report_steroids_'+runType+'.md'
results_file = 'output/'+runType+'/'+now.strftime("%y%m%d_%H%M")+'_result_steroids_'+runType+'.pickle'
summaries_file = 'output/'+runType+'/'+now.strftime("%y%m%d_%H%M")+'_summary_steroids_'+runType+'.pickle'

f = open(report_file, 'w')
f.write("# SOM Calibration grid run "+now.strftime("%d/%m/%y %H:%M")+"\n\n"+
        "Run type = "+runType+"\n\n"+
        "## Params \n\n"+
         text)
f.close()

## Computation
results = pd.DataFrame()

# Print computation header
f = open(report_file, 'a')

f.write("## Computation \n\n"+
        "### Times \n\n")

startTime = dt.now()
f.write("startTime = "+dt.isoformat(startTime)+"\n")


## Execution loop
f.write("\n### Runs \n\n")
for dim in dim_range:
    iterations = []
    for n_iter in n_iter_range:
        params = {
            'n_iter' : n_iter, 
            'x' : dim,
            'y' : int(np.ceil(const.golden*dim)),
            'dimension' : len(attributes),
            'learning_rate' : learning_rate,
            'neighborhood_size' : ((1./2)*int(np.ceil(const.golden*dim)), 1) 
        }
        print params
        f.write(str(params)+"\n")
        
        errors = pd.DataFrame()
        for i in nb_exec:
            somSteroids = som.SOM(matrix_steroids, attributes, params)
            errors[i] = somSteroids.error
        iterations.append(errors)
        
    results[dim] = iterations
results.index = n_iter_range
f.close()


# Add the times to the output file
stopTime = dt.now()
duration = stopTime - startTime

with open(report_file, "r") as f:
    contents = f.readlines()
    contents.insert(17, "stopTime = "+dt.isoformat(stopTime)+"\n"+
                        "duration = "+str(duration)+"\n")

with open(report_file, "w") as f:
    f.writelines(contents)
    f.close()
    
os.system('say "Bravo! Computation ended!"')


# ## Résultats

# In[20]:

# X = dimension (dim)
# Y = Nb of iterations (n_iter)
results


# In[29]:

# Show the first processed map
# results[dim][n_iter]
# X = Execution number
# Y = Iteration number
dim_0 = results.columns[0]
n_iter_0 = results.index[0]
print "dim : "+str(dim_0)
print "n_iter : "+str(n_iter_0)
results[dim_0][n_iter_0]


# In[30]:

results.to_pickle(results_file)


# ## Résumés des résultats

# In[42]:

summaries = pd.DataFrame()

for size in results:
    iterations = []
    for run in results[size]:
        summary = pd.DataFrame()
        run = run.transpose()

        upper_quartile = run.quantile(.75)
        lower_quartile = run.quantile(.25)
        iqr = upper_quartile - lower_quartile
        upper_whisker = run[run<=upper_quartile+1.5*iqr].max()
        lower_whisker = run[run>=lower_quartile-1.5*iqr].min()

        summary["lower_whisker_error"] = lower_whisker
        summary["upper_whisker_error"] = upper_whisker
        summary["median_error"] = run.median()
        summary["slope"] = slopeLine(summary.index, summary["median_error"])
        #summary["angle"] = angleLine(summary.index, summary["slope"])

        iterations.append(summary)
    summaries[size] = iterations
summaries.index = n_iter_range


# In[43]:

# The number of different SOM sizes
len(summaries)


# In[44]:

# X = dimension (dim)
# Y = Nb of iterations (n_iter)
summaries


# In[45]:

# Show the first processed map summary
# summaries[dim][n_iter]
# X = Execution number
# Y = Iteration number
dim_0 = summaries.columns[0]
n_iter_0 = summaries.index[0]
print "dim : "+str(dim_0)
print "n_iter : "+str(n_iter_0)
summaries[dim_0][n_iter_0]


# In[46]:

summaries.to_pickle(summaries_file)

