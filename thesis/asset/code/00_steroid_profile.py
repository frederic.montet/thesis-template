
# coding: utf-8

# # 00 - Steroid Profile

# In[15]:

import pandas as pd
import numpy as np
import pylab as pl
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import hashlib as h
get_ipython().magic(u'matplotlib inline')


# ## Import the data

# In[33]:

file = "data/data_heig.dta"
data = pd.read_stata(file)

# The row above 1130 (1-based and including header) contain individuals which seem 
# to be parents of other included in the study but who were not included themselves.
data = data.iloc[:1129]


# ## Hash each row to have an ID 

# In[34]:

hash_id = data.apply(lambda x: h.sha1(str(tuple(x))).hexdigest(), axis = 1)
hash_id.head()
hash_id.shape


# ## Select the metabolites

# In[35]:

variables = [
        'cr_andro_d', 'cr_etio_d', 'cr_saad3a17b_d', 'cr_ll_oxo_etio_d', 'cr_llb_oh_andro_d', 'cr_llb_oh_etio_d', 'cr_dha_d',
        'cr_s_ad_17b_d', 'cr_l6a_oh_dha_d', 'cr_s_at_d', 'cr_s_pt_d', 'cr_testosterone_d', 'cr_sa_dihydrotest_d',
        'cr_estriol_d', 'cr_l7b_estradiol_d', 'cr_pd_d', 'cr_pt_d', 'cr_pt_one_d',
        'cr_ths_d', 'cr_thaldo_d', 'cr_thdoc_d', 'cr_tha_d', 'cr_thb_d', 'cr_sa_thb_d', 'cr_l8_oh_tha_d',
        'cr_cortisone_d', 'cr_the_d', 'cr_a_cortolone_d', 'cr_b_cortolone_d', 'cr_zoa_dhe_d', 'cr_zob_dhe_d',
        'cr_cortisol_d', 'cr_thf_d', 'cr_sa_thf_d', 'cr_a_cortol_d', 'cr_b_cortol_d', 'cr_zoa_dhf_d', 'cr_zob_dhf_d',
        'cr_sb_oh_f_d', 'cr_l8_oh_f_d'
]

variables = [v.lower() for v in variables]
print len(variables)


# ## Raw data overview

# In[36]:

steroids = data[variables]


# In[37]:

steroids.iloc[:10,:]


# In[38]:

steroids.shape


# ## Fill missing with mean

# In[39]:

steroids = steroids.fillna(steroids.mean())


# ## Normalize

# In[40]:

# Z-Score
steroids = (steroids - steroids.mean()) / (steroids.std())


# In[41]:

steroids.iloc[:10,:]


# ## Per-variable boxplot

# In[42]:

plt.figure(figsize=(20, 10))
plt.ylim((-4, 6))
steroids.boxplot();
plt.xticks(rotation=90)
_ = pl.savefig("img/00_boxplots_raw_steroids.png", bbox_inches = 'tight', dpi=300)


# ## Save steroids raw data

# In[44]:

steroids.to_pickle('output/00_steroids_raw.pickle')
hash_id.to_pickle('output/00_hash_id.pickle')

